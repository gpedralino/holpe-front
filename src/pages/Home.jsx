import React from "react";

import { Grid, Button, Typography } from "@material-ui/core";
import "../stylesheets/home.css";

import logoHolpe from "../assets/logo_holpe_blue.png";
import logoIcmc from "../assets/logo-icmc.png";
import logoMonitora from "../assets/logo-monitora.png";
import heart from "../assets/icons/heart-solid.svg";
import holdingHeart from "../assets/icons/hand-holding-heart-solid.svg";

const Home = () => {
  return (
    <Grid container>
      <Grid
        item
        container
        direction="column"
        alignItems="center"
        className="background"
      >
        <Grid
          item
          container
          direction="column"
          alignItems="center"
          className="title-container"
        >
          <img src={logoHolpe} alt="Holpe" className="main-logo" />
          <Typography variant="subtitle1" color="primary">
            Help with Holpe
          </Typography>
        </Grid>
        <Button
          className="action-btn"
          variant="contained"
          color="secondary"
          style={{ backgroundColor: "#7ee3d5", color: "#6d2d54" }}
        >
          <img
            src={holdingHeart}
            alt="hand holding a heart"
            className="btn-icon"
          />{" "}
          Trabalhar como voluntário(a)
        </Button>
        <Button className="action-btn" variant="contained" color="secondary">
          <img src={heart} alt="heart" className="btn-icon" />
          Encontrar voluntários(as)
        </Button>
      </Grid>
      <Grid
        item
        container
        className="footer"
        justify="center"
        alignItems="center"
      >
        <span>Uma parceria entre</span>{" "}
        <img src={logoIcmc} alt="icmc" className="logo" />{" "}
        <img src={logoMonitora} alt="Monitora" className="logo" />
      </Grid>
    </Grid>
  );
};

export default Home;
