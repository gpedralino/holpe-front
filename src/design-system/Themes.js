import { createMuiTheme } from "@material-ui/core/styles";

export const holpe = createMuiTheme({
  palette: {
    primary: {
      main: "#208bb3",
    },
    secondary: {
      main: "#6d2d54",
    },
  },
  typography: {
    fontFamily: [
      "Roboto",
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ],
    subtitle1: {
      fontWeight: 100,
      fontSize: "4vw",
      textAlign: "center",
    },
  },
});
