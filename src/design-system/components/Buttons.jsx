import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

export const NavButton = withStyles({
  root: {
    height: "inherit",
    width: "calc(3vh * 6)",
    fontFamily: "Roboto",
    fontWeight: 100,
    fontSize: "3vh",
    textTransform: "none",
    color: "white",
    "&:hover": {
      height: "inherit",
      backgroundColor: "rgba(0, 0, 0, 0.4)",
      borderRadius: "0px",
    },
  },
})(Button);

export const LoginButton = withStyles({
  root: {
    minHeight: "30px",
    width: "140px",
    fontSize: "1.9vh",
    color: "#0179A7",
    background:
      "transparent linear-gradient(180deg, #FFFFFF 0%, #AEF5FA 100%) 0% 0% no-repeat padding-box;",
    "&:hover": {
      background:
        "transparent linear-gradient(180deg, #FFFFFFE6 0%, #AEF5FAE6 100%) 0% 0% no-repeat padding-box;",
    },
  },
})(Button);
