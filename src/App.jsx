import React from "react";
import ReactDOM from "react-dom";
import { Router } from "@reach/router";

import { ThemeProvider } from "@material-ui/core/styles";
import { holpe } from "./design-system/Themes";
import "./stylesheets/app.css";

import Navbar from "./common/Navbar";
import Home from "./pages/Home";

const App = () => (
  <div>
    <ThemeProvider theme={holpe}>
      <Navbar />
      <Router style={{ position: "fixed", top: "9vh", width: "100vw" }}>
        <Home path="/" />
      </Router>
    </ThemeProvider>
  </div>
);

ReactDOM.render(<App />, document.getElementById("root"));
