import React, { Fragment } from "react";

import { Grid } from "@material-ui/core";
import { NavButton, LoginButton } from "../design-system/components/Buttons";
import { Appbar } from "../design-system/components/Appbar";
import "../stylesheets/navbar.css";

import logo from "../assets/logo_holpe_white.png";

const Navbar = () => {
  return (
    <Fragment>
      <Appbar container alignItems="center">
        <Grid item container xs={12} md={10} className="navbar">
          <img src={logo} alt="logo" className="logo" />
          <NavButton className="page-btn">Home</NavButton>
          <NavButton className="page-btn">Sobre</NavButton>
          <NavButton className="page-btn">Trabalhos</NavButton>
          <NavButton className="page-btn">Contato</NavButton>
        </Grid>
        <Grid item container md={2} alignItems="center" justify="center">
          <LoginButton>LOGIN</LoginButton>
        </Grid>
      </Appbar>
    </Fragment>
  );
};

export default Navbar;
